import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_application_2/src/counter/app.dart';
import 'package:flutter_application_2/src/counter/counter_observer.dart';
import 'src/app.dart';

void main() {
  Bloc.observer = CounterObserver();
  runApp(CounterApp());
}
