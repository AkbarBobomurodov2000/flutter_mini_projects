import 'package:flutter_bloc/flutter_bloc.dart';

enum ChangeWordEvent { change }

class ChangeWordBloc extends Bloc<ChangeWordEvent, String> {
  ChangeWordBloc(String initialState) : super(initialState);

  Stream<String> mapEventToState(event) async* {
    switch (event) {
      case ChangeWordEvent.change:
        yield state + '1';
        break;
      default:
    }
  }
}
