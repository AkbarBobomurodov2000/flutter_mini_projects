import '../resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import '../models/album_model.dart';

class AlbumBloc {
  final _repository = Repository();
  final _albumFetcher = PublishSubject<AlbumModel>();

  Observable<AlbumModel> get oneAlbum => _albumFetcher.stream;

  fetchAlbum() async {
    AlbumModel albumModel = await _repository.fetchAlbum();
    _albumFetcher.sink.add(albumModel);
  }

  dispose() {
    _albumFetcher.close();
  }
}

final bloc = AlbumBloc();
