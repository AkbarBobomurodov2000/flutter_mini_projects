import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_2/src/blocs/counter_bloc.dart';
import 'package:flutter_application_2/src/cubits/counter_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CounterBloc, int>(
      builder: (context, counter) {
        
        return Column(
          children: [
            TextButton(
              onPressed: () {
                context.read<CounterBloc>().add(CounterEvent.increment);
              },
              child: Text('Increment'),
            ),
            TextButton(
              onPressed: () {},
              child: Text('Decrement'),
            ),
            Text(
              counter.toString(),
              style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                  color: Colors.redAccent),
            )
          ],
        );
      },
    );
  }
}
