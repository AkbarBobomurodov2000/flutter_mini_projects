import 'package:flutter/material.dart';
import '../models/album_model.dart';
import '../blocs/album_bloc.dart';

class AlbumList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    bloc.fetchAlbum();
    

    return Scaffold(
      appBar: AppBar(
        title: Text('Popular movies'),
      ),
      body: StreamBuilder(
        stream: bloc.oneAlbum,
        builder: ( context, AsyncSnapshot<AlbumModel> snapshot){
          if(snapshot.hasData){
            return buildAlbum(snapshot);
          }else if( snapshot.hasError ){
           return Text(snapshot.error.toString());
          }

          return Center( 
            child: CircularProgressIndicator()
           );
        },
      ),
    );
  }

  Widget buildAlbum( AsyncSnapshot<AlbumModel> snapshot ){
     return Text( snapshot.data.title );
  }
}