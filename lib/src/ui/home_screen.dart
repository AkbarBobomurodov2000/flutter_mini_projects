import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Center(
      child:Column(
        children: [
          TextButton(
            onPressed: (){
              Navigator.pushNamed(context, '/albumList');
            },
            child: Text('Albums'),
          ),
          TextButton(
            onPressed: (){
              Navigator.pushNamed(context, '/posts');
            },
            child: Text('Posts'),
          ),
        ],
      ) ,
    );
  }
}