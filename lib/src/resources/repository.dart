import 'dart:async';
import 'album_api_provider.dart';
import '../models/album_model.dart';

class Repository {
   
 final albumApiProvider=AlbumApiProvider();

 Future<AlbumModel> fetchAlbum ()=>albumApiProvider.fetchAlbum();

}