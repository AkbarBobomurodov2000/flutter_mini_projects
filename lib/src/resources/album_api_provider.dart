import 'dart:async';
import 'package:http/http.dart' show Client;
import 'dart:convert';
import '../models/album_model.dart';


class AlbumApiProvider{

  Client client  = Client();

  Future<AlbumModel> fetchAlbum() async {
    final response = await client.get('https://jsonplaceholder.typicode.com/albums/1');
    if( response.statusCode == 200 ){
        return AlbumModel.fromJson( json.decode( response.body) );
    }else{
      throw Exception('Yuklashda Error');
    }
  }

}

