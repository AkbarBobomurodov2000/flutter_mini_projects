import 'package:flutter/material.dart';
import 'package:flutter_application_2/src/blocs/album_bloc.dart';
import 'package:flutter_application_2/src/blocs/counter_bloc.dart';
import 'package:flutter_application_2/src/ui/home_screen.dart';
import 'package:flutter_application_2/src/ui/posts_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './ui/album_list.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData.dark(),
      home: HomeScreen(),
      routes: {
        '/albumList': (context) => AlbumList(),
        '/posts': (context) => BlocProvider(
            create: (BuildContext context) => CounterBloc(),
            child: PostsScreen())
      },
    );
  }
}
