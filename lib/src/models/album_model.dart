class AlbumModel {
  int id;
  int userId;
  String title;

  AlbumModel({this.id, this.userId, this.title});

  factory AlbumModel.fromJson(Map<String, dynamic> parsedJson) {
    return AlbumModel(
        id: parsedJson['id'],
        userId: parsedJson['userId'],
        title: parsedJson['title']);
  }
}
