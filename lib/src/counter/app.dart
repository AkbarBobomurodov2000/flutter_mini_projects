import 'package:flutter/material.dart';
import 'package:flutter_application_2/src/counter/view/counter_page.dart';

class CounterApp extends MaterialApp {
  CounterApp() : super(home: CounterPage());
}
